# apt-as-user
APT has Super Cow Powers (TM) and allows users to specify alternatives for configuration and runtime
directories. This functionality enables non-root users to use APT independent of the system wide configuration.

This repository contain some scripts which support users in utilizing this functionality.

# Limitations
The normal user privilege restrictions apply, therefore installation of packages will **NOT** be possible
without root permissions.

# Usage

The `bin/` directory contains symlinks to the `apt-user.sh` script with same name as the APT system utilities.
Add the `bin/` directory to path and they will be used instead of the system provided ones. Simply execute:

```shell
. ./activate.sh
```

# How it works
APT allows specifying an alternative configuration file by using the environment variable `APT_CONFIG`.
This configuration file allows overriding the default APT directories with custom ones, which are writable
by a non-root users. This is sufficient for e.g. downloading package list and packages.
