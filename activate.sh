#!/bin/sh

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
export PATH="$(readlink -e "${SCRIPT_DIR}/bin"):${PATH}"