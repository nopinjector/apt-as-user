#!/bin/bash -e

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
SCRIPT_NAME="$(basename "$0")"

function get_temp_folder {
    local uid=$(id -u)
    local temp=${TMPDIR:-/tmp}
    local dir="${temp}/apt-user.${uid}"

    if [ ! -d "${dir}" ]; then
      mkdir -m 0700 "${dir}"
      "${SCRIPT_DIR}/create-apt-dir.sh" --copy --base-dir "${dir}"
    fi

    echo "${dir}"
}

case "${SCRIPT_NAME}" in
    apt-get|apt-cache)
    ;;
    *)
      echo "usage: (apt-get|apt-cache) [args...]" >&2 && exit 1
    ;;
esac

APT_CONFIG=${APT_CONFIG:-"$(get_temp_folder)/apt.conf"}

export APT_CONFIG
"/usr/bin/${SCRIPT_NAME}" $@

