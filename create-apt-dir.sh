#!/bin/bash -e

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

APT_DIR_SYSTEM="/etc/apt"

function print_usage_and_exit {
  echo "usage: $0 [--copy] --base-dir <base-directory>"
  exit 1
}

function parse_arguments {
  while [ $# -ge 1 ]
  do
    case $1 in
        --copy)
          COPY=1
          shift
        ;;
        --base-dir)
          BASE_DIR="$2"
          shift
          shift
        ;;
        *)
          print_usage_and_exit
        ;;
    esac
  done

  if [ -z ${BASE_DIR+x} ]; then
    print_usage_and_exit
  fi
}

parse_arguments "$@"

BASE_DIR="$(readlink -f "${BASE_DIR}")"

if [ ! -z ${COPY+x} ]; then
  cp -r "${APT_DIR_SYSTEM}"/* "${BASE_DIR}/"
else
  mkdir "${BASE_DIR}"

  touch "${BASE_DIR}/sources.list" \
        "${BASE_DIR}/apt.conf"
  mkdir "${BASE_DIR}/apt.conf.d" \
        "${BASE_DIR}/preferences.d" \
        "${BASE_DIR}/trusted.gpg.d" \
        "${BASE_DIR}/sources.list.d"
fi

mkdir "${BASE_DIR}/.state" \
      "${BASE_DIR}/.cache"

cat >> "${BASE_DIR}/apt.conf" <<EOF
Dir {
  State "${BASE_DIR}/.state";
  Cache "${BASE_DIR}/.cache";
  Etc "${BASE_DIR}";
};
EOF